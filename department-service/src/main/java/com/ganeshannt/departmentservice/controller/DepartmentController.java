package com.ganeshannt.departmentservice.controller;

import com.ganeshannt.departmentservice.entity.Department;
import com.ganeshannt.departmentservice.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/departments")
@Slf4j
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @PostMapping("/")
    public Department saveDepartment(@RequestBody Department department) {
        log.info("Department controller - Inside save department function ");
        return departmentService.save(department);
    }

    @GetMapping("/{id}")
    public Department findByDepartmentId(@PathVariable("id") Long id) {
        log.info("Department controller - Inside get department function ");
        return departmentService.findByDepartmentId(id);
    }
}
