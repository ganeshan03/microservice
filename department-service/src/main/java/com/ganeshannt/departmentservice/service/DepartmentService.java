package com.ganeshannt.departmentservice.service;

import com.ganeshannt.departmentservice.entity.Department;
import com.ganeshannt.departmentservice.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department save(Department department) {
        log.info("Department service - Inside save department function ");
        return departmentRepository.save(department);
    }

    public Department findByDepartmentId(Long id) {
        log.info("Department service - Inside get department function ");
        if (departmentRepository.existsById(id)) {
            return departmentRepository.findById(id).get();
        }
        return null;
    }
}
