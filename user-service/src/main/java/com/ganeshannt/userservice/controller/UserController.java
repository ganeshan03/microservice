package com.ganeshannt.userservice.controller;

import com.ganeshannt.userservice.VO.ResponseTemplateVO;
import com.ganeshannt.userservice.entity.User;
import com.ganeshannt.userservice.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public User saveUser(@RequestBody User user) {
        log.info("User controller - Inside save user method");
        return userService.save(user);
    }

    @GetMapping("/{id}")
    public ResponseTemplateVO getUserWithDepartment(@PathVariable("id") Long userId) {
        log.info("User controller - Inside getUserWithDepartment method");
        return userService.getUserWithDepartment(userId);
    }
}
